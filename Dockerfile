FROM python:3

RUN apt-get update \
  && apt-get install -y groff jq \
  && pip install --upgrade pip \
  && pip install --upgrade awscli boto boto3 \
  && apt-get clean -y \
  && apt-get autoremove -y \
  && rm -rfv /var/cache/apt \
  && rm -rf /root/.cache

ENTRYPOINT []
CMD ["/bin/bash"]
