#!/usr/bin/env sh

if [ -z "${CI_COMMIT_SHA}" ]; then
  >&2 echo "CI_COMMIT_SHA not defined."
  exit 1
fi
if [ -z "${CI_COMMIT_REF_SLUG}" ]; then
  >&2 echo "CI_COMMIT_REF_SLUG not defined."
  exit 1
fi

# First 7 characters of the sha hash, similar to the output of `git rev-parse --short HEAD`.
SHA_SHORT="$(expr substr "${CI_COMMIT_SHA}" 1 7)"

# IMAGE_URL is set by .gitlab-ci.yml - If run locally, it'll use 'aws-cli'
IMAGE_NAME="${IMAGE_URL:-aws-cli}"
DOCKER_URL="${IMAGE_NAME}:${CI_COMMIT_SHA}"
DOCKER_URL_SHORT="${IMAGE_NAME}:${SHA_SHORT}"
DOCKER_URL_BRANCH="${IMAGE_NAME}:latest-${CI_COMMIT_REF_SLUG}"

# Attempt to pull the latest branch build
docker pull "${DOCKER_URL_BRANCH}"

docker build \
  --cache-from "${DOCKER_URL_BRANCH}" \
  --tag "${DOCKER_URL}" \
  "${PWD}"

# Push to the registry
docker push "${DOCKER_URL}"

# Also tag and push the short form
docker tag "${DOCKER_URL}" "${DOCKER_URL_SHORT}"
docker push "${DOCKER_URL_SHORT}"

# Also tag and push the branch name tag
docker tag "${DOCKER_URL}" "${DOCKER_URL_BRANCH}"
docker push "${DOCKER_URL_BRANCH}"
